//! Handle batteries.
//!
//! This module manages and interprets sysfs entries related to batteries.
//!
//! Further reading:
//! - [power_supply class attributes units][1]
//! - [How to access sysfs information][2]
//!
//! [1]: https://www.kernel.org/doc/html/latest/power/power_supply_class.html
//! [2]: https://www.kernel.org/doc/html/latest/admin-guide/sysfs-rules.html

pub use battery::Battery;
pub use battery::BatteryStats;
pub use battery::BatteryStatsRaw;
pub use health::Health;
pub use soc::SoC;

pub mod battery;
pub mod health;
pub mod soc;
