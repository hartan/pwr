use crate::error::PwrError;
use colored::*;

pub struct SoC {
    value: f32,
}

impl SoC {
    pub fn new(value: f32) -> Result<Self, PwrError> {
        if !(0.0..=1.0).contains(&value) {
            return Err(PwrError::InvalidSoC(value));
        }

        Ok(SoC { value })
    }
}

impl std::fmt::Display for SoC {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let soc_p: u32 = f32::round(self.value * 100.0) as u32;
        let string = format!("{:3} %", soc_p);
        let string = match soc_p {
            0..=15 => string.red().bold(),
            16..=40 => string.yellow(),
            41..=100 => string.green(),
            _ => string.black().strikethrough(),
        };
        write!(f, "{}", string)
    }
}
