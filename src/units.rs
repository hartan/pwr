//! Physical units for batteries and power supplies.
//!
use std::fmt;
use uom::si::{electric_current::ampere, electric_potential::volt, f32::*, power::watt};

/// Convenience macro for displaying units.
///
/// The actual unit and unit conversion is done by the `uom` crate. This merely serves as a wrapper
/// to implement [`fmt::Display`] on such that the values along with their units can be
/// auto-formatted. Formatting is done such that the value is in the range 0.001..1000 with its
/// respective unit.
#[derive(PartialEq, Debug)]
pub enum Unit {
    /// Volt
    V(ElectricPotential),
    /// Ampere
    A(ElectricCurrent),
    /// Watt
    W(Power),
    /// Joule, TODO
    J,
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Unit::V(v) => {
                let val = v.get::<volt>() as f32;
                if val >= 1.0e3 {
                    write!(f, "{:.3} kV", val / 1.0e3)
                } else if val <= 1.0e-3 {
                    write!(f, "{:.3} mV", val * 1.0e3)
                } else {
                    write!(f, "{:.3} V", val)
                }
            }
            Unit::A(a) => {
                let val = a.get::<ampere>() as f32;
                if val >= 1.0e3 {
                    write!(f, "{:.3} kA", val / 1.0e3)
                } else if val <= 1.0e-3 {
                    write!(f, "{:.3} mA", val * 1.0e3)
                } else {
                    write!(f, "{:.3} A", val)
                }
            }
            Unit::W(w) => {
                let val = w.get::<watt>() as f32;
                if val >= 1.0e3 {
                    write!(f, "{:.3} kW", val / 1.0e3)
                } else if val <= 1.0e-3 {
                    write!(f, "{:.3} mW", val * 1.0e3)
                } else {
                    write!(f, "{:.3} W", val)
                }
            }
            _ => write!(f, "N/A"),
        }
    }
}
