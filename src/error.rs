//! Error types
use std::path::PathBuf;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum PwrError {
    #[error("sysfs entry under {0} not accessible.")]
    NoSysfs(PathBuf),

    #[error("An error occured")]
    Error,

    #[error(transparent)]
    GenericError(#[from] std::io::Error),

    #[error("Failed to parse: {0}")]
    ParseError(String),

    #[error("Not implemented yet: {0}")]
    NotImplemented(String),

    #[error("Not a valid SoC: {0} %")]
    InvalidSoC(f32),

    #[error("Not a valid battery health: {0} %")]
    InvalidHealth(f32),

    #[error("No battery found under: {0}")]
    NoBattery(PathBuf),

    #[error("Couldn't parse invalid 'Type' from {0}")]
    ParseTypeError(String),
}
