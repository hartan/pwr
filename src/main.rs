use anyhow::{Context, Result};

pub fn main() -> Result<()> {
    pretty_env_logger::init();
    pwr::run().context("An error occured".to_string())
}
